package marsroverkata.enums;

public enum Direction {
	N("N"), 
	S("S"), 
	E("E"), 
	W("W");
	
	private final String prefix;
	
	Direction(String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return prefix;
	}
}
