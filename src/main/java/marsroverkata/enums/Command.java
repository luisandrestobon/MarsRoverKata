package marsroverkata.enums;

public enum Command {
	F('f'),
	B('b'),
	L('l'),
	R('r');
	
	private final char chrCommand;
	
	Command(char chrCommand) {
		this.chrCommand = chrCommand;
	}

	public char getChrCommand() {
		return chrCommand;
	}
	
	public static Command getCommandByChrCommand(char chrCommand) {
		for (Command command : Command.values()) {
			if (command.getChrCommand() == chrCommand) {
				return command;
			}
		}
		return null;
	}
}
