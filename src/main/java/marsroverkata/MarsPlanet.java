package marsroverkata;

public interface MarsPlanet {
	public boolean isAnObstacle(int x, int y);
	public int getMaxEdge();
}
