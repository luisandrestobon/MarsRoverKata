package marsroverkata;

import java.awt.Point;

import marsroverkata.enums.Direction;

public interface MarsRover {
	public Point receiveCommand(char[] chrCommands);
	public Point getActualPoint();
	public Direction getActualDirection();
}
