package marsroverkata.commandexecution;

import java.awt.Point;

import marsroverkata.enums.Command;
import marsroverkata.enums.Direction;

public interface CommandExecution {
	public boolean accept(Command actualCommand, Direction actualDirection);
	public Direction execute(Point initialPoint);
}
