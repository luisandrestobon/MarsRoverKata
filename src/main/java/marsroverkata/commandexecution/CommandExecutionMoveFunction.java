package marsroverkata.commandexecution;

import java.awt.Point;

public interface CommandExecutionMoveFunction {
	public void moveFunction(Point actualPoint);
}
