package marsroverkata.commandexecution;

import java.awt.Point;

import marsroverkata.enums.Command;
import marsroverkata.enums.Direction;

public class CommandExecutionImpl implements CommandExecution {
	
	private Command innerCommand;
	private Direction innerDirection;
	private CommandExecutionMoveFunction innerFunction;
	private Direction nextDirection;
	
	private CommandExecutionImpl(
			Command innerCommand, 
			Direction innerDirection, 
			CommandExecutionMoveFunction innerFunction,
			Direction nextDirection) {
		this.innerCommand = innerCommand;
		this.innerDirection = innerDirection;
		this.innerFunction = innerFunction;
		this.nextDirection = nextDirection;
	}

	@Override
	public boolean accept(Command actualCommand, Direction actualDirection) {
		return ( actualCommand.equals(innerCommand) && actualDirection.equals(innerDirection) );
	}

	@Override
	public Direction execute(Point initialPoint) {
		this.innerFunction.moveFunction(initialPoint);
		return this.nextDirection;
	}
	
	public static CommandExecutionImpl fromCommandDirectionFunctionAndNextDirection(
			Command innerCommand, 
			Direction innerDirection, 
			CommandExecutionMoveFunction innerFunction,
			Direction nextDirection) {
		return new CommandExecutionImpl(innerCommand, innerDirection, innerFunction, nextDirection);
	}

}
