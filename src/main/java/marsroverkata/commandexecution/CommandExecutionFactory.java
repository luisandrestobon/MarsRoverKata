package marsroverkata.commandexecution;

import java.util.Arrays;
import java.util.List;

import marsroverkata.enums.Command;
import marsroverkata.enums.Direction;

public class CommandExecutionFactory {
	
	public static List<CommandExecution> createExecutionList() {
		return Arrays.asList(
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.F, Direction.N, point -> point.y++, Direction.N),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.F, Direction.S, point -> point.y--, Direction.S),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.F, Direction.E, point -> point.x++, Direction.E),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.F, Direction.W, point -> point.x--, Direction.W),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.B, Direction.N, point -> point.y--, Direction.N),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.B, Direction.S, point -> point.y++, Direction.S),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.B, Direction.E, point -> point.x--, Direction.E),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.B, Direction.W, point -> point.x++, Direction.W),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.R, Direction.N, point -> {}, Direction.E),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.R, Direction.S, point -> {}, Direction.W),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.R, Direction.E, point -> {}, Direction.S),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.R, Direction.W, point -> {}, Direction.N),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.L, Direction.N, point -> {}, Direction.W),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.L, Direction.S, point -> {}, Direction.E),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.L, Direction.E, point -> {}, Direction.N),
				CommandExecutionImpl.fromCommandDirectionFunctionAndNextDirection(Command.L, Direction.W, point -> {}, Direction.S)
				);
	}
	
}
