package marsroverkata;

import java.awt.Point;
import java.util.List;

import marsroverkata.commandexecution.CommandExecution;
import marsroverkata.commandexecution.CommandExecutionFactory;
import marsroverkata.enums.Command;
import marsroverkata.enums.Direction;

public class MarsRoverImpl implements MarsRover {
	
	private static final String MSG_ERROR_EMPTY_OR_NULL_CHAR_ARRAY = "There must be at least one character into the array";
	private static final String MSG_ERROR_ILLEGAL_CHAR = "Wrong command character";
	private static final int DEFAULT_EDGE = 5;
	
	private MarsPlanet marsPlanet;
	
	private Point actualPoint;
	
	private Direction actualDirection;
	
	private List<CommandExecution> allCommandExecution = CommandExecutionFactory.createExecutionList();

	public MarsRoverImpl(
			MarsPlanet marsPlanet,
			Point initialPoint, 
			Direction initialDirection) {
		this.marsPlanet = marsPlanet;
		this.actualPoint = initialPoint;
		this.actualDirection = initialDirection;
	}

	public Point receiveCommand(char[] chrCommands) {
//		System.out.println("*************************");
//		System.out.println(chrCommands);
//		System.out.println("*************************");
		char chrCommand;
		Point obstaclePoint = null;
		validateChrCommands(chrCommands);
		Command command = null;
		for (int i = 0; i < chrCommands.length; i++) {
			chrCommand = chrCommands[i];
			command = convertCharToCommand(chrCommand);
			executeAction(command);
			obstaclePoint = null;
			if (isAnActualObstacle()) {
				obstaclePoint = new Point(actualPoint);
				executeAction(
						(command == Command.F) ? 
								Command.B : 
								Command.F);
				break;
			}
			// Here we put the real Mars Rover movement order
			// However, we're not implementing that part of the flow
		}
		return obstaclePoint;
	}

	private void validateChrCommands(char[] chrCommands) {
		if (chrCommands == null || chrCommands.length <= 0) {
			throw new RuntimeException(MSG_ERROR_EMPTY_OR_NULL_CHAR_ARRAY);
		}
		for (int i = 0; i < chrCommands.length; i++) {
			convertCharToCommand(chrCommands[i]);
		}
	}
	
	private Command convertCharToCommand(char chrCommand) {
		Command command = Command.getCommandByChrCommand(chrCommand);
		if (command == null) {
			throw new RuntimeException(MSG_ERROR_ILLEGAL_CHAR);
		}
		return command;
	}
	
	private void executeAction(Command command) {
//		System.out.println("---------------------");
//		System.out.println("command: " + command);
//		System.out.println(this.actualDirection);
//		System.out.println(this.actualPoint);
		for (CommandExecution commandExecution : allCommandExecution) {
			if (commandExecution.accept(command, this.actualDirection)) {
				this.actualDirection = commandExecution.execute(this.actualPoint);
				break;
			}
		}
//		System.out.println(this.actualDirection);
//		System.out.println(this.actualPoint);
//		System.out.println("---------------------");
		transformEdges();
	}

	private void transformEdges() {
		int edge = this.marsPlanet.getMaxEdge() > 0 ? this.marsPlanet.getMaxEdge() : DEFAULT_EDGE;
		if (actualPoint.x == edge) {
			actualPoint.x = 1 - edge;
		} else if (actualPoint.x == -edge) {
			actualPoint.x = edge - 1;
		}
		if (actualPoint.y == edge) {
			actualPoint.y = 1 - edge;
		} else if (actualPoint.y == -edge) {
			actualPoint.y = edge - 1;
		}
	}

	private boolean isAnActualObstacle() {
		return marsPlanet.isAnObstacle(this.actualPoint.x, this.actualPoint.y);
	}

	public void setMarsPlanet(MarsPlanet marsPlanet) {
		this.marsPlanet = marsPlanet;
	}

	public Point getActualPoint() {
		return this.actualPoint;
	}

	public Direction getActualDirection() {
		return this.actualDirection;
	}
	
}
