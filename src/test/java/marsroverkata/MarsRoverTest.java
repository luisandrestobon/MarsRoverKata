package marsroverkata;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.awt.Point;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import marsroverkata.enums.Direction;

public class MarsRoverTest {
	
	private static final char F = 'f';
	private static final char B = 'b';
	private static final char R = 'r';
	private static final char L = 'l';
	
	private MarsPlanet marsPlanet = mock(MarsPlanet.class);
	
	private MarsRover marsRover;
	
	private Point obstaclePoint;
	
	@Test
	void receiveCommand_givenCommandNForwardOnce_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);
		
		whenReceiveCommand(new char[] { F });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 1));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
	}

	@Test
	void receiveCommand_givenCommandNBackwardOnce_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { B });
		
		thenValidateActualPointWithExpectedValue(new Point(0, -1));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
	}
	
	@Test
	void receiveCommand_givenCommandNRightOnce_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { R });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.E);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNLeftOnce_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { L });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.W);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNForwardx2_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { F, F });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 2));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNBackwardx2_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { B, B });
		
		thenValidateActualPointWithExpectedValue(new Point(0, -2));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNRightx2_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { R, R });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.S);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNLeftx2_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { L, L });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.S);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNForwardxEDGE_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirectionWitEdge(new Point(0, 0), Direction.N, 5);

		whenReceiveCommand(new char[] { F, F, F, F, F });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 1 - 5));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNBackwardxEDGE_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirectionWitEdge(new Point(0, 0), Direction.N, 5);

		whenReceiveCommand(new char[] { B, B, B, B, B });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 5 - 1));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}

	@Test
	void receiveCommand_givenCommandEForwardxEDGE_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirectionWitEdge(new Point(0, 0), Direction.E, 5);

		whenReceiveCommand(new char[] { F, F, F, F, F });
		
		thenValidateActualPointWithExpectedValue(new Point(1 - 5, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.E);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandEBackwardxEDGE_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirectionWitEdge(new Point(0, 0), Direction.E, 5);

		whenReceiveCommand(new char[] { B, B, B, B, B });
		
		thenValidateActualPointWithExpectedValue(new Point(5 - 1, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.E);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNRoute1_whenNoObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommand(new char[] { F, F, L, F, F, L, F, R, B });
		
		thenValidateActualPointWithExpectedValue(new Point(-1, 1));
		thenValidateActualDirectionWithExpectedValue(Direction.W);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenCommandNForwardOnce_whenObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirectionWithObstacle(new Point(0, 0), Direction.N, new Point(0, 1));

		whenReceiveCommand(new char[] { F });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(new Point(0, 1));
		
	}
	
	@Test
	void receiveCommand_givenCommandNBackwardOnce_whenObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirectionWithObstacle(new Point(0, 0), Direction.N, new Point(0, -1));

		whenReceiveCommand(new char[] { B });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(new Point(0, -1));
		
	}
	
	@Test
	void receiveCommand_givenCommandNRoute1_whenObstacle_thenOk() {
		
		givenAMarsRoverAtInitialPointAndDirectionWithObstacle(new Point(0, 0), Direction.N, new Point(-2, 2));

		whenReceiveCommand(new char[] { F, F, L, F, F, L, F, R, B });
		
		thenValidateActualPointWithExpectedValue(new Point(-1, 2));
		thenValidateActualDirectionWithExpectedValue(Direction.W);
		thenValidateActualObstacleWithExpectedValue(new Point(-2, 2));
		
	}
	
	@Test
	void receiveCommand_givenEmptyCommand_whenValidate_thenException() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommandWithErrors(new char[] {  });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenWrongCommandSize1_whenValidate_thenException() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommandWithErrors(new char[] { 'z' });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenWrongCommandSizeNLast_whenValidate_thenException() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommandWithErrors(new char[] { F, F, L, F, F, L, F, R, 'q' });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	@Test
	void receiveCommand_givenWrongCommandSizeNMiddle_whenValidate_thenException() {
		
		givenAMarsRoverAtInitialPointAndDirection(new Point(0, 0), Direction.N);

		whenReceiveCommandWithErrors(new char[] { F, F, L, F, 't', L, F, R, B });
		
		thenValidateActualPointWithExpectedValue(new Point(0, 0));
		thenValidateActualDirectionWithExpectedValue(Direction.N);
		thenValidateActualObstacleWithExpectedValue(null);
		
	}
	
	private void thenValidateActualPointWithExpectedValue(Point expectedPoint) {
		assertEquals(expectedPoint.x, marsRover.getActualPoint().x);
		assertEquals(expectedPoint.y, marsRover.getActualPoint().y);
	}
	
	private void thenValidateActualDirectionWithExpectedValue(Direction expectedDirection) {
		assertEquals(expectedDirection, marsRover.getActualDirection());
	}
	
	private void thenValidateActualObstacleWithExpectedValue(Point expectedObstaclePoint) {
		if (expectedObstaclePoint == null) {
			assertNull(obstaclePoint);
		} else {
			assertNotNull(obstaclePoint);
		}
	}

	private void givenAMarsRoverAtInitialPointAndDirection(Point initialPoint, Direction initialDirection) {
		marsRover = new MarsRoverImpl(marsPlanet, initialPoint, initialDirection);
	}

	private void whenReceiveCommand(char[] chrCommands) {
		obstaclePoint = marsRover.receiveCommand(chrCommands);
	}
	
	private void whenReceiveCommandWithErrors(char[] chrCommands) {
		Assertions.assertThrows(RuntimeException.class, () -> {
			obstaclePoint = marsRover.receiveCommand(chrCommands);
		});
	}
	
	private void givenAMarsRoverAtInitialPointAndDirectionWitEdge(Point initialPoint, Direction initialDirection, int edge) {
		when(marsPlanet.getMaxEdge())
			.thenReturn(edge);
		givenAMarsRoverAtInitialPointAndDirection(initialPoint, initialDirection);
	}
	
	private void givenAMarsRoverAtInitialPointAndDirectionWithObstacle(Point initialPoint, Direction initialDirection, Point obstaclePoint) {
		when(marsPlanet.isAnObstacle(obstaclePoint.x, obstaclePoint.y))
			.thenReturn(true);
		givenAMarsRoverAtInitialPointAndDirection(initialPoint, initialDirection);
	}

}
