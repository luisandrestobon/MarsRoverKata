package marsroverkata;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.awt.Point;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import marsroverkata.enums.Direction;

class MarsRoverImplTest {
	
	private static final int EDGE = 5;
	private static final char F = 'f';
	private static final char B = 'b';
	private static final char R = 'r';
	private static final char L = 'l';
	private static final String MSG_ERR_OBSTACLE = "There is an obstacle";
	private static final String MSG_ERR_NOT_OBSTACLE = "There is not an obstacle";
	
	private MarsPlanet marsPlanet;
	
	@BeforeEach
	void init() {
		marsPlanet = mock(MarsPlanet.class);
		when(marsPlanet.getMaxEdge())
			.thenReturn(EDGE);
	}
	
	private Point receiveCommandGeneralTest(
			Point initialPoint,
			Point finalPoint,
			Direction initialDirection,
			Direction finalDirection,
			char[] chrCommands) {
		// when
		MarsRover marsRover = new MarsRoverImpl(marsPlanet, initialPoint, initialDirection);
		Point obstaclePoint = marsRover.receiveCommand(chrCommands);
		
		// then
		assertEquals(finalPoint.x, marsRover.getActualPoint().x);
		assertEquals(finalPoint.y, marsRover.getActualPoint().y);
		assertEquals(finalDirection, marsRover.getActualDirection());
		return obstaclePoint;
	}
	
	private void receiveCommandGeneralTestWithObstacles(
			Point initialPoint,
			Point finalPoint,
			Point obstaclePoint,
			Direction initialDirection,
			Direction finalDirection,
			char[] chrCommands) {
		when(marsPlanet.isAnObstacle(obstaclePoint.x, obstaclePoint.y))
			.thenReturn(true);
		Point actualObstaclePoint = receiveCommandGeneralTest(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
		assertNotNull(actualObstaclePoint, MSG_ERR_NOT_OBSTACLE);
		assertEquals(obstaclePoint.x, actualObstaclePoint.x);
		assertEquals(obstaclePoint.y, actualObstaclePoint.y);
	}
	
	private void receiveCommandGeneralTestWithoutObstacles(
			Point initialPoint,
			Point finalPoint,
			Direction initialDirection,
			Direction finalDirection,
			char[] chrCommands) {
		when(marsPlanet.isAnObstacle(Mockito.anyInt(), Mockito.anyInt()))
			.thenReturn(false);
		Point obstaclePoint = receiveCommandGeneralTest(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
		assertNull(obstaclePoint, MSG_ERR_OBSTACLE);
	}
	
	@Test
	void getActualPoint_givenAllValues_whenNoMoves_thenOk() {
		Point initialPoint = null;
		Point finalPoint = null;
		Direction initialDirection = Direction.N;
		MarsRover marsRover = null;
		for (int i = 0; i < EDGE; i++) {
			for (int j = 0; j < EDGE; j++) {
				// given
				initialPoint = new Point(i, j);
				marsRover = new MarsRoverImpl(marsPlanet, initialPoint, initialDirection);
				
				// when
				finalPoint = marsRover.getActualPoint();
				
				// then
				assertEquals(finalPoint.x, marsRover.getActualPoint().x);
				assertEquals(finalPoint.y, marsRover.getActualPoint().y);
			}
		}
	}
	
	@Test
	void getActualDirection_givenAllValues_whenNoMoves_thenOk() {
		Point initialPoint = new Point(0, 0);
		Direction initialDirection = null;
		Direction finalDirection = null;
		MarsRover marsRover = null;
		Direction[] directions = Direction.values();
		for (Direction direction : directions) {
			// given
			initialDirection = direction;
			marsRover = new MarsRoverImpl(marsPlanet, initialPoint, initialDirection);
			
			// when
			finalDirection = marsRover.getActualDirection();
			
			// then
			assertEquals(finalDirection, initialDirection);
		}
	}
	
	@Test
	void receiveCommand_givenCommandNForwardOnce_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, 1);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.N;
		char[] chrCommands = { F };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNBackwardOnce_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, -1);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.N;
		char[] chrCommands = { B };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNRightOnce_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, 0);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.E;
		char[] chrCommands = { R };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNLeftOnce_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, 0);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.W;
		char[] chrCommands = { L };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNForwardx2_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, 2);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.N;
		char[] chrCommands = { F, F };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNBackwardx2_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, -2);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.N;
		char[] chrCommands = { B, B };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNRightx2_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, 0);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.S;
		char[] chrCommands = { R, R };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNLeftx2_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, 0);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.S;
		char[] chrCommands = { L, L };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}

	@Test
	void receiveCommand_givenCommandNForwardxEDGE_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, 1 - EDGE);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.N;
		char[] chrCommands = { F, F, F, F, F };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNBackwardxEDGE_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(0, EDGE - 1);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.N;
		char[] chrCommands = { B, B, B, B, B };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandEForwardxEDGE_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(1 - EDGE, 0);
		Direction initialDirection = Direction.E;
		Direction finalDirection = Direction.E;
		char[] chrCommands = { F, F, F, F, F };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandEBackwardxEDGE_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(EDGE - 1, 0);
		Direction initialDirection = Direction.E;
		Direction finalDirection = Direction.E;
		char[] chrCommands = { B, B, B, B, B };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNRoute1_whenNoObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(-1, 1);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.W;
		char[] chrCommands = { F, F, L, F, F, L, F, R, B };
		receiveCommandGeneralTestWithoutObstacles(
				initialPoint, 
				finalPoint, 
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNForwardOnce_whenObstacle_thenOk() {
		// given
			Point initialPoint = new Point(0, 0);
			Point finalPoint = new Point(0, 0);
			Point obstaclePoint = new Point(0, 1);
			Direction initialDirection = Direction.N;
			Direction finalDirection = Direction.N;
			char[] chrCommands = { F };
			receiveCommandGeneralTestWithObstacles(
					initialPoint,
					finalPoint,
					obstaclePoint,
					initialDirection, 
					finalDirection, 
					chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNBackwardOnce_whenObstacle_thenOk() {
		// given
			Point initialPoint = new Point(0, 0);
			Point finalPoint = new Point(0, 0);
			Point obstaclePoint = new Point(0, -1);
			Direction initialDirection = Direction.N;
			Direction finalDirection = Direction.N;
			char[] chrCommands = { B };
			receiveCommandGeneralTestWithObstacles(
					initialPoint,
					finalPoint,
					obstaclePoint,
					initialDirection, 
					finalDirection, 
					chrCommands);
	}
	
	@Test
	void receiveCommand_givenCommandNRoute1_whenObstacle_thenOk() {
		// given
		Point initialPoint = new Point(0, 0);
		Point finalPoint = new Point(-1, 2);
		Point obstaclePoint = new Point(-2, 2);
		Direction initialDirection = Direction.N;
		Direction finalDirection = Direction.W;
		char[] chrCommands = { F, F, L, F, F, L, F, R, B };
		receiveCommandGeneralTestWithObstacles(
				initialPoint,
				finalPoint,
				obstaclePoint,
				initialDirection, 
				finalDirection, 
				chrCommands);
	}
	
	@Test
	void receiveCommand_givenEmptyCommand_whenValidate_thenException() {
		// given
		Assertions.assertThrows(RuntimeException.class, () -> {
			Point initialPoint = new Point(0, 0);
			Point finalPoint = new Point(0, 0);
			Direction initialDirection = Direction.N;
			Direction finalDirection = Direction.N;
			char[] chrCommands = {  };
			receiveCommandGeneralTestWithoutObstacles(
					initialPoint, 
					finalPoint, 
					initialDirection, 
					finalDirection, 
					chrCommands);
		});
	}
	
	@Test
	void receiveCommand_givenWrongCommandSize1_whenValidate_thenException() {
		
		Assertions.assertThrows(RuntimeException.class, () -> {
			Point initialPoint = new Point(0, 0);
			Point finalPoint = new Point(0, 0);
			Direction initialDirection = Direction.N;
			Direction finalDirection = Direction.N;
			char[] chrCommands = { 'z' };
			receiveCommandGeneralTestWithoutObstacles(
					initialPoint, 
					finalPoint, 
					initialDirection, 
					finalDirection, 
					chrCommands);
		});
		
	}
	
	@Test
	void receiveCommand_givenWrongCommandSizeNLast_whenValidate_thenException() {
		// given
		Assertions.assertThrows(RuntimeException.class, () -> {
			Point initialPoint = new Point(0, 0);
			Point finalPoint = new Point(0, 0);
			Direction initialDirection = Direction.N;
			Direction finalDirection = Direction.N;
			char[] chrCommands = { F, F, L, F, F, L, F, R, 'q' };
			receiveCommandGeneralTestWithoutObstacles(
					initialPoint, 
					finalPoint, 
					initialDirection, 
					finalDirection, 
					chrCommands);
		});
	}
	
	@Test
	void receiveCommand_givenWrongCommandSizeNMiddle_whenValidate_thenException() {
		// given
		Assertions.assertThrows(RuntimeException.class, () -> {
			Point initialPoint = new Point(0, 0);
			Point finalPoint = new Point(0, 0);
			Direction initialDirection = Direction.N;
			Direction finalDirection = Direction.N;
			char[] chrCommands = { F, F, L, F, 't', L, F, R, B };
			receiveCommandGeneralTestWithoutObstacles(
					initialPoint, 
					finalPoint, 
					initialDirection, 
					finalDirection, 
					chrCommands);
		});
	}
	
}
